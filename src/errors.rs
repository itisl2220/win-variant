use std::string::FromUtf16Error;

use thiserror::Error;
use widestring::NulError;
use winapi::shared::winerror::{DISP_E_BADINDEX, E_INVALIDARG, E_OUTOFMEMORY};
use winapi::{shared::wtypes::VARTYPE, um::winnt::HRESULT};

#[derive(Debug, Error)]
pub enum HResultError {
    #[error("bad index in array")]
    DispatchBadIndex,
    #[error("invalid argument")]
    InvalidArgument,
    #[error("not enough memory for data")]
    OutofMemory,
    #[error("unknown hresult code `{0}`")]
    UnknownHResult(HRESULT),
}

impl From<HRESULT> for HResultError {
    fn from(hr: HRESULT) -> Self {
        match hr {
            DISP_E_BADINDEX => HResultError::DispatchBadIndex,
            E_INVALIDARG => HResultError::InvalidArgument,
            E_OUTOFMEMORY => HResultError::OutofMemory,
            _ => HResultError::UnknownHResult(hr),
        }
    }
}

#[derive(Debug, Error)]
pub enum VariantArgError {
    #[error("wide char parsing error: {0}")]
    ToWideCharArrayError(#[from] NulError<u16>),
}

#[derive(Debug, Error)]
pub enum VariantResultError {
    #[error("boolean can only be min or max of an i16, got `{0}`")]
    InvalidBool(i16),
    #[error("The pointer was null")]
    NullPointer,
    #[error("UTF-16 parse error: {0}")]
    FromUtf16(#[from] FromUtf16Error),
    #[error("unsupported type conversion to type `{0}`")]
    UnsupportedTypeConversion(VARTYPE),
}

#[derive(Debug, Error)]
pub enum SafeArrayError {
    #[error("safe array dimension count does not match desired type")]
    DimensionMismatch,
    #[error("failed to create safe array")]
    FailedToCreateSafeArray,
    #[error("safe array vartype does not match desired type `{0}`")]
    IncorrectDataType(VARTYPE),
    #[error("safe array pointer is null")]
    NullPointer,
    #[error("hresult error `{0}`")]
    HResultError(HResultError),
    #[error("wide char parsing error: {0}")]
    ToWideCharArrayError(#[from] NulError<u16>),
    #[error("UTF-16 parse error: {0}")]
    FromUtf16(#[from] FromUtf16Error),
}

impl From<HRESULT> for SafeArrayError {
    fn from(hr: HRESULT) -> Self {
        SafeArrayError::HResultError(HResultError::from(hr))
    }
}
